

/* Mobile Menu
============================================================*/
$(document).on('click', '.nav_menu_btn, .nav_menu_bg', function () {
	$('.nav_menu_btn').toggleClass('active');
	$('.nav_menu').toggleClass('active');
	$('.nav_menu_bg').toggleClass('active');
	$('html, body').toggleClass('ov-h');
});
/*========== Mobile Menu ==========*/










/* Banner
============================================================*/
$(document).ready(function () {
	try {
		var owl = $('.banner');

		owl.owlCarousel({
			// autoPlay: 7000,
			stopOnHover: true,
			singleItem: true,
			autoHeight : true,
			navigation: false,
			pagination: false,
			slideSpeed: 300,
			paginationSpeed: 400,
			addClassActive: true,
			afterAction: function () {
				$('.banner_bg').removeClass('active').eq(this.owl.currentItem).addClass('active');
			}
		});

		// Custom Navigation Events
		$('.banner_btn.next').click(function () {
			owl.trigger('owl.next');
		});

		$('.banner_btn.prev').click(function () {
			owl.trigger('owl.prev');
		});
	}
	catch (e) {
		console.warn('Owl Carousel cannot find .banner');
	}
});
/*========== Banner ==========*/





/* Partners
============================================================*/
$(document).ready(function () {
	try {
		var owl = $('.partner_bnr');

		owl.owlCarousel({
			// autoPlay : 3000,
			stopOnHover: true,
			navigation: false,
			pagination: true,
			itemsCustom: [
				[0, 2],
				[768, 3],
				[1200, 4]
			],
			lazyLoad: true
		});
	}
	catch (e) {
		console.warn("Owl Carousel cannot find .partner_bnr");
	}
});
/*========== Partners ==========*/









/* Slice Flower
============================================================*/
$(window).on('load', function () {
	try {
		$('.slice_f_gal').masonry({
			itemSelector: '.slice_f_gal_it',
			percentPosition: true,
		});
	}
	catch (e) {
		console.warn('Masonry cannot find .slice_f_gal');
	}
});
/*========== Slice Flower ==========*/









/* Checkbox
============================================================*/
$(document).on('click', '.reg_pop_chk', function () {
	$(this).toggleClass('active');

	var inputCheck = $(this).siblings('.reg_pop_chk_inp');
	inputCheck.prop('checked', !inputCheck.prop('checked'));
});
/*========== Checkbox ==========*/






/* Registration Popup
============================================================*/
$(document).on('click', '[data-login]', function (e) {
	e.preventDefault();

	$('html, body').animate({ scrollTop: 0 }, 700);

	$('.hdr_act_user').toggleClass('active');
});

$(document).click(function(e) {
	if ($(e.target).is('.hdr_act_user, .hdr_act_user *, [data-login], [data-login] *') === false) {
		$('.hdr_act_user').removeClass('active');
	}
});
/*========== Registration Popup ==========*/












/* Search Product Check
============================================================*/
$(document).on('click', '.search_p_nav_chk', function () {
	$(this).toggleClass('active');

	var inputCheck = $(this).children('.inp');
	inputCheck.prop('checked', !inputCheck.prop('checked'));
});
/*========== Search Product Check ==========*/





/* Search Product Dropdown
============================================================*/
$(document).on('click', '.search_p_nav_opt_ls > li > a', function (e) {
	e.preventDefault();

	$(this).parent().toggleClass('active').siblings().removeClass('active');
});

$(document).click(function(e) {
	if ($(e.target).is('.search_p_nav_opt_ls > li, .search_p_nav_opt_ls > li *') === false) {
		$('.search_p_nav_opt_ls > li').removeClass('active');
	}
});
/*========== Search Product Dropdown ==========*/




/* Search Product Inner Check
============================================================*/
$(document).on('click', '.search_p_nav_drop > li', function () {
	$(this).toggleClass('active');

	var inputCheck = $(this).children('.inp');
	inputCheck.prop('checked', !inputCheck.prop('checked'));


	$(this).siblings().removeClass('active').children('.inp').prop('checked', false);



	if ($(this).hasClass('active'))
	{
		$(this).parent().siblings('a').addClass('active');
	}
	else
	{
		$(this).parent().siblings('a').removeClass('active');
	}
});
/*========== Search Product Inner Check ==========*/








/* Flower Action
============================================================*/
$(document).on('click', '.flower_act', function () {
	$(this).toggleClass('active');
});
/*========== Flower Action ==========*/







/* Flower Remove
============================================================*/
$(document).on('click', '.flower_close', function () {
	$(this).closest('[data-flower-close]').fadeOut(700, function () {
		$(this).remove();
	});
});
/*========== Flower Remove ==========*/







/* Personal Data Edit
============================================================*/
$(document).on('click', '[data-personal-data]', function (e) {
	e.preventDefault();

	$(this).closest('.psnl_data_it').fadeOut(700, function () {
		$(this).siblings('.psnl_data_it').fadeIn(700);
	});
});
/*========== Personal Data Edit ==========*/







/* Personal Data Tab
============================================================*/
$(document).on('click', '.psnl_data_ls > li', function () {

	$(this).addClass('active').siblings().removeClass('active');

	$('.data_tab_it').eq($(this).index()).addClass('active').siblings().removeClass('active');
});
/*========== Personal Data Tab ==========*/








/* Formalization Info
============================================================*/
$(document).on('click', '[data-code]', function (e) {
	e.preventDefault();

	$('[data-code-active]').attr('disabled', true);

	$('[data-code-disabled]').attr('disabled', false).focus();

	$('.fmlz_info_code_txt').addClass('active');
});
/*========== Formalization Info ==========*/








/* Formalization Pay Type
============================================================*/
$(document).on('click', '.fmlz_pay_type_it', function () {
	$('.fmlz_pay_type_it').removeClass('active').children('.inp').prop('checked', false);

	$(this).addClass('active').children('.inp').prop('checked', true);
});
/*========== Formalization Pay Type ==========*/










/* Product Slider
============================================================*/
try {
	$('.prod_img_bnr').slick({
		asNavFor: '.prod_img_thumb_bnr',
		slidesToShow: 1,
		slidesToScroll: 1,
		adaptiveHeight: true,
		infinite: false,
		arrows: false,
		fade: true
	});


	$('.prod_img_thumb_bnr').slick({
		asNavFor: '.prod_img_bnr',
		slidesToShow: 3,
		slidesToScroll: 1,
		vertical: true,
		verticalSwiping: true,
		infinite: false,
		focusOnSelect: true,
		prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-up"></i></button>',
		nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-down"></i></button>',

		responsive: [
			{
				breakpoint: 480,
				settings: {
					vertical: false,
					verticalSwiping: false,
					slidesToShow: 2,
				}
			}
		]
	});
}
catch (e) {
	console.warn('Slick cannot find .prod_img_bnr, .prod_img_thumb_bnr');
}
/*========== Product Slider ==========*/





/* Product Zoom
============================================================*/
$(document).ready(function () {
	try {
		for (var i = 0; i < $('.xzoom').length; i++)
		{
			$('.xzoom').eq(i).xzoom({
				tint: '#333',
				Xoffset: 15
			});
		}
	}
	catch (e) {
		console.warn('xZoom cannot find .xzoom');
	}
});
/*========== Product Zoom ==========*/







/* Product Pay Type
============================================================*/
$(document).on('click', '.prod_pay_it', function () {
	$('.prod_pay_it').removeClass('active').children('.inp').prop('checked', false);

	$(this).addClass('active').children('.inp').prop('checked', true);
});
/*========== Product Pay Type ==========*/











/* Registration Modal
============================================================*/
$('#RegModal_1').on('hidden.bs.modal', function (e) {
	setTimeout(function () {
		$('#RegModal_2').modal('show');
	}, 500);
});
/*========== Registration Modal ==========*/



/* Password Recovery Modal
============================================================*/
$('#PassRecovery').on('hidden.bs.modal', function (e) {
	setTimeout(function () {
		$('#PassNew').modal('show');
	}, 500);
});
/*========== Password Recovery Modal ==========*/

















/* Header
============================================================*/

/*========== Header ==========*/


